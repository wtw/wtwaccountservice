# base image
FROM python:3.8.7-slim-buster
# setup environment variable
ENV DockerHOME=/wtwaccountservice
ENV HOST=docker.for.mac.host.internal
# set work directory
RUN mkdir -p $DockerHOME
# where your code lives
WORKDIR $DockerHOME

COPY wtwaccountservice/requirements.txt .
ADD wtwaccountservice $DockerHOME


# set environment variables
# install dependencies
RUN pip3 install --upgrade pip
# copy whole project to your docker home directory. COPY . $DockerHOME
# run this command to install all dependencies
RUN pip3 install -r requirements.txt

# port where the Django app runs
EXPOSE 8000
# start server
CMD python manage.py runserver 0.0.0.0:8000