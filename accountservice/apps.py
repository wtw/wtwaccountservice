from django.apps import AppConfig


class AccountserviceConfig(AppConfig):
    name = 'accountservice'
